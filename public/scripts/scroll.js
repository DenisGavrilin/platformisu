

function alignment(){

    //thead
    let firstTrHeight = window.getComputedStyle(document.getElementsByClassName('top_title')[0]).height;
    let secondTr = document.getElementsByClassName('bottom_title')[0];
    let tdElems = secondTr.getElementsByTagName('td');
    for (let i = 0; i < tdElems.length; i++) {
        tdElems[i].style.top = firstTrHeight;
        
    }

    //direction
    let theadHeight = window.getComputedStyle(document.getElementsByTagName('thead')[0]).height;
    let dirs = document.getElementsByClassName('direction');
    for (let i = 0; i < dirs.length; i++) {
        dirs[i].style.top = theadHeight;
    }

}